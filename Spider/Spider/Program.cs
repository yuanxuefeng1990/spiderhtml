﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Threading;
using System.Net;
using System.IO;
using pzkj_api.Utility;

namespace Spider
{
    class Program
    {

        static void Main(string[] args)
        {

            //Timer timer = new Timer(spiderHtml,"",1000,5000);
            //Console.ReadLine();
            //timer.Dispose();

            //  Spider51cto();
            //spider5SingHtml("http://5sing.kugou.com/fc/14713396.html");
            //Console.ReadLine();

            //分类
            spider111Html("http://www.111.com.cn/categories/");
            Console.ReadLine();

            //商品列表
            //productlist();
            //Console.ReadLine();
        }


        /// <summary>
        /// 分类
        /// </summary>
        /// <param name="url"></param>
        private static void spider111Html(string url)
        {
            string xpathDiv = "//div[@class='alonesort']";
            HtmlWeb hw = new HtmlWeb();
            hw.OverrideEncoding = Encoding.GetEncoding("gbk");  //此处需要设置页面编码格式
            HtmlAgilityPack.HtmlDocument doc = hw.Load(url);

            HtmlNodeCollection nodelist = doc.DocumentNode.SelectNodes(xpathDiv);

            for (int i = 0; i < nodelist.Count; i++)
            {
                //string catename = nodelist[i].InnerText.Trim();
                HtmlNode node = nodelist[i];

                //一级
                string topCate = node.SelectSingleNode("div/h3/a").InnerText;
                string href = "http:" + node.SelectSingleNode("div/h3/a").Attributes["href"].Value;
                string strsql = "IF NOT EXISTS( SELECT * FROM dbo.category WHERE href='" + href + "') BEGIN INSERT dbo.category(name, parentId,href)VALUES  ( '" + topCate + "', 0,'" + href + "');SELECT @@IDENTITY end ELSE SELECT id FROM dbo.category WHERE href = '" + href + "'";
                int id = Convert.ToInt32(DbHelperSQL.GetSingle(strsql));


                HtmlNodeCollection mcNode = node.SelectNodes("div[@class='mc']/dl");

                if (mcNode != null)
                {
                    for (int k = 0; k < mcNode.Count; k++)
                    {
                        //二级
                        HtmlNode cate2Node = mcNode[k].SelectSingleNode("dt/a");
                        if (cate2Node != null)
                        {
                            string parentName = cate2Node.InnerText;
                            string href2 = "http:" + cate2Node.Attributes["href"].Value;
                            string strsqlp = "IF NOT EXISTS( SELECT * FROM dbo.category WHERE href='" + href2 + "') BEGIN INSERT dbo.category(name, parentId,href)VALUES  ( '" + parentName + "'," + id + ",'" + href2 + "');SELECT @@IDENTITY end ELSE SELECT id FROM dbo.category WHERE href = '" + href2 + "'";
                            int pid = Convert.ToInt32(DbHelperSQL.GetSingle(strsqlp));


                            //三级
                            HtmlNodeCollection childNodes = mcNode[k].SelectNodes("dd/em/a");
                            foreach (HtmlNode cNode in childNodes)
                            {
                                string childName = cNode.InnerText;
                                string href3 = "http:" + cNode.Attributes["href"].Value;
                                string strsqlExsist = "SELECT * FROM dbo.category WHERE href='" + href3 + "'";
                                bool issuccess = DbHelperSQL.Exists(strsqlExsist);
                                if (issuccess == false)
                                {
                                    string strsqlc = "INSERT dbo.category(name, parentId,href)VALUES  ( '" + childName + "'," + pid + ",'" + href3 + "');SELECT @@IDENTITY;";
                                    int cateid = Convert.ToInt32(DbHelperSQL.GetSingle(strsqlc));
                                    productlist(href3, cateid);
                                }
                            }
                        }
                    }
                }

            }
            Console.WriteLine("done!");
        }


        private static void productlist(string url, int cateid)
        {
            //string url = "http://www.111.com.cn/categories/953785-j1";
            string xpathDiv = "//*[contains(@id,'producteg_')]";
            string pageXpath = "//span[@class='pageOp']";

            HtmlWeb hw = new HtmlWeb();
            hw.OverrideEncoding = Encoding.GetEncoding("gbk");  //此处需要设置页面编码格式
            HtmlAgilityPack.HtmlDocument doc = hw.Load(url);
            HtmlNode pageNode = doc.DocumentNode.SelectSingleNode(pageXpath);
            if (pageNode == null)
            {
                return;
            }
            string pagenum = System.Text.RegularExpressions.Regex.Replace(pageNode.InnerText, @"[^0-9]+", "");
            int page = 0;
            if (pagenum != null)
            {
                page = Convert.ToInt32(pagenum);
            }

            url = url.Substring(0, url.Length - 1);
            for (int i = 1; i <= page; i++)
            {
                HtmlAgilityPack.HtmlDocument moredoc = hw.Load(url + i);
                HtmlNodeCollection nodelist = moredoc.DocumentNode.SelectNodes(xpathDiv);
                if (nodelist != null)
                {

                    foreach (HtmlNode node in nodelist)
                    {
                        HtmlNode divNode = node.SelectSingleNode("div[@class='itemSearchResultCon']");
                        int id = Convert.ToInt32(divNode.Attributes["itemid"].Value);
                        string exsistsql = "SELECT * FROM dbo.produtc WHERE comcode='" + id + "'";
                        if (!DbHelperSQL.Exists(exsistsql))
                        {
                            string detailUrl = "http://www.111.com.cn/product/" + id + ".html";
                            HtmlAgilityPack.HtmlDocument detaildoc = hw.Load(detailUrl);
                            string infoXPath = "//div[@class='goods_intro']";
                            HtmlNode infoNode = detaildoc.DocumentNode.SelectSingleNode(infoXPath);

                            string proname = infoNode.SelectSingleNode("table/tr[1]/td").InnerText;
                            proname = proname.Replace("  ", " ").Replace("'", "");
                            string[] titles = proname.Split(' ');
                            if (titles.Length == 1)
                            {
                                proname = titles[0];
                            }
                            else if (titles.Length == 2)
                            {
                                proname = titles[0];
                            }
                            else
                                proname = titles[1];

                            string brand = infoNode.SelectSingleNode("table/tr[2]/td[1]").InnerText;
                            brand = brand.Replace("'", "");
                            string spe = infoNode.SelectSingleNode("table/tr[2]/td[2]").InnerText;
                            spe = spe.Replace("'", "");
                            string weight = infoNode.SelectSingleNode("table/tr[3]/td[1]").InnerText;
                            weight = weight.Replace("'", "");
                            string factory = infoNode.SelectSingleNode("table/tr[3]/td[2]").InnerText;
                            factory = factory.Replace("'", "");
                            string auth_code = infoNode.SelectSingleNode("table/tr[4]/td[1]").InnerText.Trim();
                            auth_code = auth_code.Replace("'", "");
                            //if (auth_code.Length > 9)
                            //{
                            //    auth_code = auth_code.Substring(0, auth_code.Length - 9);
                            //}

                            HtmlNode otcNode = infoNode.SelectSingleNode("table/tr[4]/td[2]");
                            string isotc = "";
                            if (otcNode != null)
                                isotc = otcNode.InnerText.Trim().Replace("'", "");

                            string insertsql = "INSERT dbo.produtc( comcode ,name ,spe ,brand ,weight ,factory ,auth_code , isotc,cate)VALUES  (" + id + ",'" + proname + "','" + spe + "','" + brand + "','" + weight + "','" + factory + "','" + auth_code + "','" + isotc + "','" + cateid + "' )";
                            DbHelperSQL.ExecuteSql(insertsql);

                            HtmlNode pictureNode = detaildoc.DocumentNode.SelectSingleNode("//img[@id='productImg']");
                            string picurl = "";
                            if (pictureNode != null)
                            {
                                picurl = pictureNode.Attributes["src"].Value;
                            }

                            HtmlNode thumbNode = detaildoc.DocumentNode.SelectSingleNode("//a[@class='zoomThumbActive']/img");
                            string thumbnail = "";
                            if (thumbNode != null)
                            {
                                thumbnail = thumbNode.Attributes["src"].Value;
                            }


                            string picsql = "INSERT dbo.picture( comcode, bigPicture, thumbnail )VALUES  ( " + id + ",'" + picurl + "','" + thumbnail + "' )";
                            DbHelperSQL.ExecuteSql(picsql);
                        }
                    }
                }
            }


        }


        private static List<_51cto> Spider51cto()
        {
            Console.Clear();
            string HtmlDocPath = "//html/body/div/div[4]/div[6]/div";
            string itemPath = "/div[1]/div[2]/h3/a";
            List<_51cto> list = new List<_51cto>();
            HtmlWeb htmlweb = new HtmlWeb();
            HtmlDocument document = htmlweb.Load(@"http://www.cnblogs.com/");
            HtmlNodeCollection anchors = document.DocumentNode.SelectNodes(HtmlDocPath);
            HtmlNode temp = null;
            _51cto cto = null;
            foreach (HtmlNode anchor in anchors)
            {
                cto = new _51cto();
                temp = HtmlNode.CreateNode(anchor.OuterHtml);
                string strtemp = temp.SelectSingleNode(itemPath).InnerHtml;
                cto.title = strtemp;
                Console.WriteLine(cto.title);
            }

            return list;
        }

        private static void spider5SingHtml(string url)
        {
            Console.Clear();
            if (url.Contains("fc"))
            {
                //title xpath
                string titlepath = "/html/body/div[3]/div[1]/div[1]/div/h1";
                //singer xpath
                string singerpath = "/html/body/div[3]/div[1]/div[1]/p/strong/a";
                //Shakelight xpath
                string shakelightpath = "/html/body/div[3]/div[1]/div[1]/p/a";
                //type xpath
                string typepath = "/html/body/div[3]/div[1]/div[1]/p/em[3]";
                //song xpath 
                string songpath = "/html/body/div[10]/embed";
                HtmlWeb htmlweb = new HtmlWeb();
                HtmlDocument document = htmlweb.Load(@"http://5sing.kugou.com/fc/14540628.html");
                HtmlNode title = document.DocumentNode.SelectSingleNode(titlepath);
                Console.WriteLine(title.InnerHtml);
                HtmlNode singer = document.DocumentNode.SelectSingleNode(singerpath);
                Console.WriteLine(singer.InnerHtml);
                HtmlNode shakelight = document.DocumentNode.SelectSingleNode(shakelightpath);
                Console.WriteLine(shakelight.InnerHtml);
                HtmlNode type = document.DocumentNode.SelectSingleNode(typepath);
                Console.WriteLine(type.InnerHtml);
                HtmlNode song = document.DocumentNode.SelectSingleNode("//div[class='sp_card_bg']");
                Console.WriteLine(song);
                Console.WriteLine("========");
            }
            else
            {
                string titlepath = "/html/body/div[9]/embed";
                HtmlWeb htmlweb = new HtmlWeb();
                HtmlDocument document = htmlweb.Load(@"http://5sing.kugou.com/yc/2961716.html");
                HtmlNode title = document.DocumentNode.Element(titlepath);
                Console.WriteLine(title.InnerHtml);
            }



        }

    }
}
